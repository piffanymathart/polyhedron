package Polyhedron;

import Geometry.Lines.ILine3d;
import Geometry.Lines.Line3d;
import Geometry.Polygons.IPolygon3d;
import Geometry.Polygons.Polygon3d;
import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.jupiter.api.Assertions.*;

class PolyhedronTest {

	private final Point3d P1 = new Point3d(11,12,13);
	private final Point3d P2 = new Point3d(21,22,23);
	private final Point3d P3 = new Point3d(31,32,33);
	private final Point3d P4 = new Point3d(41,42,43);

	@Test
	void defaultConstructor() {
		IPolyhedron polyhedron = new Polyhedron();
		assertArrayEquals(new Point3d[0], polyhedron.getVertices());
		assertArrayEquals(new int[0][0], polyhedron.getEdgeIndices());
		assertArrayEquals(new int[0][0], polyhedron.getFaceIndices());
	}

	@Test
	void copyConstructor() {
		Point3d[] vertices = {P1,P2,P3};
		int[][] edgeIndices = {{0,2},{2,1}};
		int[][] faceIndices = {{0,1,2},{2,0,1},{1,2,0}};
		IPolyhedron polyhedron = new Polyhedron(vertices, edgeIndices, faceIndices);

		IPolyhedron copy = new Polyhedron(polyhedron);
		assertArrayEquals(vertices, copy.getVertices());
		assertArrayEquals(edgeIndices, copy.getEdgeIndices());
		assertArrayEquals(faceIndices, copy.getFaceIndices());
	}

	@Test
	void valueConstructor() {
		Point3d[] vertices = {P1,P2,P3};
		int[][] edgeIndices = {{0,2},{2,1}};
		int[][] faceIndices = {{0,1,2},{2,0,1},{1,2,0}};
		IPolyhedron polyhedron = new Polyhedron(vertices, edgeIndices, faceIndices);

		assertArrayEquals(vertices, polyhedron.getVertices());
		assertArrayEquals(edgeIndices, polyhedron.getEdgeIndices());
		assertArrayEquals(faceIndices, polyhedron.getFaceIndices());

		int[][] invalidEdgeIndices1 = {{}};
		int[][] invalidEdgeIndices2 = {{1}};
		int[][] invalidEdgeIndices3 = {{0,2},{}};
		int[][] invalidEdgeIndices4 = {{0,1,2}};

		assertThrows(RuntimeException.class, () -> new Polyhedron(vertices, invalidEdgeIndices1, faceIndices));
		assertThrows(RuntimeException.class, () -> new Polyhedron(vertices, invalidEdgeIndices2, faceIndices));
		assertThrows(RuntimeException.class, () -> new Polyhedron(vertices, invalidEdgeIndices3, faceIndices));
		assertThrows(RuntimeException.class, () -> new Polyhedron(vertices, invalidEdgeIndices4, faceIndices));
	}

	@Test
	void getNumVertices() {
		Point3d[] verts1 = null;
		Point3d[] verts2 = {};
		Point3d[] verts3 = {P1};
		Point3d[] verts4 = {P1, P2, P3};

		int expected1 = 0;
		int expected2 = 0;
		int expected3 = 1;
		int expected4 = 3;

		IPolyhedron polyhedron1 = new Polyhedron(verts1, null, null);
		assertEquals(expected1, polyhedron1.getNumVertices());

		IPolyhedron polyhedron2 = new Polyhedron(verts2, null, null);
		assertEquals(expected2, polyhedron2.getNumVertices());

		IPolyhedron polyhedron3 = new Polyhedron(verts3, null, null);
		assertEquals(expected3, polyhedron3.getNumVertices());

		IPolyhedron polyhedron4 = new Polyhedron(verts4, null, null);
		assertEquals(expected4, polyhedron4.getNumVertices());
	}

	@Test
	void getVertices() {
		Point3d[] verts1 = null;
		Point3d[] verts2 = {};
		Point3d[] verts3 = {P1};
		Point3d[] verts4 = {P1, P2, P3};

		Point3d[] expected1 = {};
		Point3d[] expected2 = {};
		Point3d[] expected3 = {P1};
		Point3d[] expected4 = {P1, P2, P3};

		IPolyhedron polyhedron1 = new Polyhedron(verts1, null, null);
		assertArrayEquals(expected1, polyhedron1.getVertices());

		IPolyhedron polyhedron2 = new Polyhedron(verts2, null, null);
		assertArrayEquals(expected2, polyhedron2.getVertices());

		IPolyhedron polyhedron3 = new Polyhedron(verts3, null, null);
		assertArrayEquals(expected3, polyhedron3.getVertices());

		IPolyhedron polyhedron4 = new Polyhedron(verts4, null, null);
		assertArrayEquals(expected4, polyhedron4.getVertices());
	}

	@Test
	void getVertex() {
		Point3d[] verts1 = null;
		Point3d[] verts2 = {};
		Point3d[] verts3 = {P1};
		Point3d[] verts4 = {P1, P2, P3};

		IPolyhedron polyhedron1 = new Polyhedron(verts1, null, null);
		assertThrows(RuntimeException.class, () -> polyhedron1.getVertex(0));

		IPolyhedron polyhedron2 = new Polyhedron(verts2, null, null);
		assertThrows(RuntimeException.class, () -> polyhedron2.getVertex(0));

		IPolyhedron polyhedron3 = new Polyhedron(verts3, null, null);
		assertThrows(RuntimeException.class, () -> polyhedron3.getVertex(-1));
		assertEquals(P1, polyhedron3.getVertex(0));
		assertThrows(RuntimeException.class, () -> polyhedron3.getVertex(1));

		IPolyhedron polyhedron4 = new Polyhedron(verts4, null, null);
		assertThrows(RuntimeException.class, () -> polyhedron4.getVertex(-1));
		assertEquals(P1, polyhedron4.getVertex(0));
		assertEquals(P2, polyhedron4.getVertex(1));
		assertEquals(P3, polyhedron4.getVertex(2));
		assertThrows(RuntimeException.class, () -> polyhedron4.getVertex(3));
	}

	@Test
	void getNumEdges() {

		Point3d[] verts = {P1,P2,P3,P4};

		int[][] edgeInds1 = null;
		int[][] edgeInds2 = {};
		int[][] edgeInds3 = {{1,0}};
		int[][] edgeInds4 = {{1,0}, {3,2}};

		int expected1 = 0;
		int expected2 = 0;
		int expected3 = 1;
		int expected4 = 2;

		IPolyhedron polyhedron1 = new Polyhedron(verts, edgeInds1, null);
		assertEquals(expected1, polyhedron1.getNumEdges());

		IPolyhedron polyhedron2 = new Polyhedron(verts, edgeInds2, null);
		assertEquals(expected2, polyhedron2.getNumEdges());

		IPolyhedron polyhedron3 = new Polyhedron(verts, edgeInds3, null);
		assertEquals(expected3, polyhedron3.getNumEdges());

		IPolyhedron polyhedron4 = new Polyhedron(verts, edgeInds4, null);
		assertEquals(expected4, polyhedron4.getNumEdges());
	}

	@Test
	void getEdgeIndices() {

		Point3d[] verts = {P1,P2,P3,P4};

		int[][] edgeInds1 = null;
		int[][] edgeInds2 = {};
		int[][] edgeInds3 = {{1,0}};
		int[][] edgeInds4 = {{1,0}, {3,2}};

		int[][] expected1 = {};
		int[][] expected2 = {};
		int[][] expected3 = {{1,0}};
		int[][] expected4 = {{1,0}, {3,2}};

		IPolyhedron polyhedron1 = new Polyhedron(verts, edgeInds1, null);
		assertArrayEquals(expected1, polyhedron1.getEdgeIndices());

		IPolyhedron polyhedron2 = new Polyhedron(verts, edgeInds2, null);
		assertArrayEquals(expected2, polyhedron2.getEdgeIndices());

		IPolyhedron polyhedron3 = new Polyhedron(verts, edgeInds3, null);
		assertArrayEquals(expected3, polyhedron3.getEdgeIndices());

		IPolyhedron polyhedron4 = new Polyhedron(verts, edgeInds4, null);
		assertArrayEquals(expected4, polyhedron4.getEdgeIndices());
	}

	@Test
	void getEdges() {

		Point3d[] verts = {P1,P2,P3,P4};

		int[][] edgeInds1 = null;
		int[][] edgeInds2 = {};
		int[][] edgeInds3 = {{1,0}};
		int[][] edgeInds4 = {{1,0}, {3,2}};

		ILine3d[] expected1 = {};
		ILine3d[] expected2 = {};
		ILine3d[] expected3 = {new Line3d(P2,P1)};
		ILine3d[] expected4 = {new Line3d(P2,P1), new Line3d(P4,P3)};

		IPolyhedron polyhedron1 = new Polyhedron(verts, edgeInds1, null);
		assertArrayEquals(expected1, polyhedron1.getEdges());

		IPolyhedron polyhedron2 = new Polyhedron(verts, edgeInds2, null);
		assertArrayEquals(expected2, polyhedron2.getEdges());

		IPolyhedron polyhedron3 = new Polyhedron(verts, edgeInds3, null);
		assertArrayEquals(expected3, polyhedron3.getEdges());

		IPolyhedron polyhedron4 = new Polyhedron(verts, edgeInds4, null);
		assertArrayEquals(expected4, polyhedron4.getEdges());
	}

	@Test
	void getEdge() {

		Point3d[] verts = {P1,P2,P3,P4};

		int[][] edgeInds1 = null;
		int[][] edgeInds2 = {};
		int[][] edgeInds3 = {{1,0}};
		int[][] edgeInds4 = {{1,0}, {3,2}};

		IPolyhedron polyhedron1 = new Polyhedron(verts, edgeInds1, null);
		assertThrows(RuntimeException.class, () -> polyhedron1.getEdge(0));

		IPolyhedron polyhedron2 = new Polyhedron(verts, edgeInds2, null);
		assertThrows(RuntimeException.class, () -> polyhedron2.getEdge(0));

		IPolyhedron polyhedron3 = new Polyhedron(verts, edgeInds3, null);
		assertThrows(RuntimeException.class, () -> polyhedron3.getEdge(-1));
		assertEquals(new Line3d(P2,P1), polyhedron3.getEdge(0));
		assertThrows(RuntimeException.class, () -> polyhedron3.getEdge(1));

		IPolyhedron polyhedron4 = new Polyhedron(verts, edgeInds4, null);
		assertThrows(RuntimeException.class, () -> polyhedron4.getEdge(-1));
		assertEquals(new Line3d(P2,P1), polyhedron4.getEdge(0));
		assertEquals(new Line3d(P4,P3), polyhedron4.getEdge(1));
		assertThrows(RuntimeException.class, () -> polyhedron4.getEdge(2));
	}

	@Test
	void getFaceIndices() {

		Point3d[] verts = {P1, P2, P3, P4};

		int[][] faceInds1 = null;
		int[][] faceInds2 = {};
		int[][] faceInds3 = {{}};
		int[][] faceInds4 = {{1}};
		int[][] faceInds5 = {{1}, {1,2}, {1,2,3}};

		int[][] expected1 = {};
		int[][] expected2 = {};
		int[][] expected3 = {{}};
		int[][] expected4 = {{1}};
		int[][] expected5 = {{1}, {1,2}, {1,2,3}};

		IPolyhedron polyhedron1 = new Polyhedron(verts, null, faceInds1);
		assertArrayEquals(expected1, polyhedron1.getFaceIndices());

		IPolyhedron polyhedron2 = new Polyhedron(verts, null, faceInds2);
		assertArrayEquals(expected2, polyhedron2.getFaceIndices());

		IPolyhedron polyhedron3 = new Polyhedron(verts, null, faceInds3);
		assertArrayEquals(expected3, polyhedron3.getFaceIndices());

		IPolyhedron polyhedron4 = new Polyhedron(verts, null, faceInds4);
		assertArrayEquals(expected4, polyhedron4.getFaceIndices());

		IPolyhedron polyhedron5 = new Polyhedron(verts, null, faceInds5);
		assertArrayEquals(expected5, polyhedron5.getFaceIndices());
	}

	@Test
	void getFaces() {

		Point3d[] verts = {P1, P2, P3, P4};

		int[][] faceInds1 = null;
		int[][] faceInds2 = {};
		int[][] faceInds3 = {{}};
		int[][] faceInds4 = {{1}};
		int[][] faceInds5 = {{1}, {1,2}, {1,2,3}};

		IPolygon3d[] expected1 = {};
		IPolygon3d[] expected2 = {};
		IPolygon3d[] expected3 = {new Polygon3d()};
		IPolygon3d[] expected4 = {new Polygon3d(P2)};
		IPolygon3d[] expected5 = {new Polygon3d(P2), new Polygon3d(P2,P3), new Polygon3d(P2,P3,P4)};

		IPolyhedron polyhedron1 = new Polyhedron(verts, null, faceInds1);
		assertArrayEquals(expected1, polyhedron1.getFaces());

		IPolyhedron polyhedron2 = new Polyhedron(verts, null, faceInds2);
		assertArrayEquals(expected2, polyhedron2.getFaces());

		IPolyhedron polyhedron3 = new Polyhedron(verts, null, faceInds3);
		assertArrayEquals(expected3, polyhedron3.getFaces());

		IPolyhedron polyhedron4 = new Polyhedron(verts, null, faceInds4);
		assertArrayEquals(expected4, polyhedron4.getFaces());

		IPolyhedron polyhedron5 = new Polyhedron(verts, null, faceInds5);
		assertArrayEquals(expected5, polyhedron5.getFaces());
	}

	@Test
	void getFace() {

		Point3d[] verts = {P1, P2, P3, P4};

		int[][] faceInds1 = null;
		int[][] faceInds2 = {};
		int[][] faceInds3 = {{}};
		int[][] faceInds4 = {{1}};
		int[][] faceInds5 = {{1}, {1,2}, {1,2,3}};

		IPolyhedron polyhedron1 = new Polyhedron(verts, null, faceInds1);
		assertThrows(RuntimeException.class, () -> polyhedron1.getFace(0));

		IPolyhedron polyhedron2 = new Polyhedron(verts, null, faceInds2);
		assertThrows(RuntimeException.class, () -> polyhedron2.getFace(0));

		IPolyhedron polyhedron3 = new Polyhedron(verts, null, faceInds3);
		assertThrows(RuntimeException.class, () -> polyhedron3.getFace(-1));
		assertEquals(new Polygon3d(), polyhedron3.getFace(0));
		assertThrows(RuntimeException.class, () -> polyhedron3.getFace(1));

		IPolyhedron polyhedron4 = new Polyhedron(verts, null, faceInds4);
		assertThrows(RuntimeException.class, () -> polyhedron4.getFace(-1));
		assertEquals(new Polygon3d(P2), polyhedron4.getFace(0));
		assertThrows(RuntimeException.class, () -> polyhedron4.getFace(1));

		IPolyhedron polyhedron5 = new Polyhedron(verts, null, faceInds5);
		assertThrows(RuntimeException.class, () -> polyhedron5.getFace(-1));
		assertEquals(new Polygon3d(P2), polyhedron5.getFace(0));
		assertEquals(new Polygon3d(P2,P3), polyhedron5.getFace(1));
		assertEquals(new Polygon3d(P2,P3,P4), polyhedron5.getFace(2));
		assertThrows(RuntimeException.class, () -> polyhedron5.getFace(3));
	}

	@Test
	void transform() {
		Point3d[] vertices = {P1,P2,P3};
		int[][] edgeIndices = {{0,2},{2,1}};
		int[][] faceIndices = {{0,1,2},{2,0,1},{1,2,0}};
		IPolyhedron actual = new Polyhedron(vertices, edgeIndices, faceIndices);

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));
		actual.transform(mat);

		Point3d q1 = new Point3d(11+1,12+2,13+3);
		Point3d q2 = new Point3d(21+1,22+2,23+3);
		Point3d q3 = new Point3d(31+1,32+2,33+3);
		Point3d[] expectedVertices = {q1,q2,q3};

		IPolyhedron expected = new Polyhedron(expectedVertices, edgeIndices, faceIndices);
		assertEquals(expected, actual);
	}

	@Test
	void equals() {
		Point3d[] vertices1 = {P1,P2,P3,P4};
		Point3d[] vertices2 = null;
		Point3d[] vertices3 = {};
		Point3d[] vertices4 = {P1,P2,P3};
		Point3d[] vertices5 = {P1,P2,P3,P2};
		Point3d[][] allVertices = {vertices1, vertices2, vertices3, vertices4, vertices5};

		int[][] edgeInds1 = null;
		int[][] edgeInds2 = {};
		int[][] edgeInds3 = {{1,0}};
		int[][] edgeInds4 = {{1,2}};
		int[][] edgeInds5 = {{1,0}, {3,2}};
		int[][][] allEdgeInds = {edgeInds1, edgeInds2, edgeInds3, edgeInds4, edgeInds5};

		int[][] faceInds1 = {{1,2,0},{2,3,0}};
		int[][] faceInds2 = null;
		int[][] faceInds3 = {};
		int[][] faceInds4 = {{1,2,0}};
		int[][] faceInds5 = {{1,2,0},{2,3,1}};
		int[][][] allFaceInds = {faceInds1, faceInds2, faceInds3, faceInds4, faceInds5};

		IPolyhedron polyhedron00 = new Polyhedron(vertices1, edgeInds4, faceInds5);
		for(int i=0; i<allVertices.length; i++) {
			for(int j=0; j<allEdgeInds.length; j++) {
				for (int k = 0; k < allFaceInds.length; k++) {
					IPolyhedron other;
					try {
						other = new Polyhedron(allVertices[i], allEdgeInds[j], allFaceInds[k]);
					}
					catch(Exception e) {
						// if construction errs, make sure the parameters are actually invalid
						int maxInd = getMaxInd(allEdgeInds[j], allFaceInds[k]);
						int vertSize = allVertices[i]==null ? 0 : allVertices[i].length;
						assert(maxInd > vertSize-1);
						continue;
					}
					if (i == 0 && j==3 && k == 4) {
						assertEquals(polyhedron00, other);
					}
					else assertNotEquals(polyhedron00, other);
				}
			}
		}
	}

	private int getMaxInd(int[] inds) {
		int maxInd = -1;
		if(inds==null) return maxInd;
		for(int i=0; i<inds.length; i++) maxInd = Math.max(maxInd, inds[i]);
		return maxInd;
	}

	private int getMaxInd(int[][] inds) {
		int maxInd = -1;
		if(inds==null) return maxInd;
		for(int i=0; i<inds.length; i++) maxInd = Math.max(maxInd, getMaxInd(inds[i]));
		return maxInd;
	}

	private int getMaxInd(int[][] inds1, int[][] inds2) {
		return Math.max(getMaxInd(inds1), getMaxInd(inds2));
	}
}