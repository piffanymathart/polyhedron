package Polyhedron;

import Geometry.Polygons.IPolygon3d;
import Geometry.Polygons.Polygon3d;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class NormalManagerTest {

	@Test
	void getNormal() {

		Point3d P1 = new Point3d(0,0,0);
		Point3d P2 = new Point3d(1,1,0);
		Point3d P3 = new Point3d(1,1,-1);
		Point3d P4 = new Point3d(0,0,-1);

		IPolygon3d polygon = new Polygon3d(P1,P2,P3,P4);
		Vector3d actual = new NormalManager().getNormal(polygon);

		Vector3d expected = new Vector3d(-1,1,0);
		expected.normalize();

		assertEquals(expected, actual);

		assertThrows(
			RuntimeException.class,
			() -> new NormalManager().getNormal(new Polygon3d(P1,P2,P1))
		);

		assertThrows(
			RuntimeException.class,
			() -> new NormalManager().getNormal(new Polygon3d(P1,P2,P1,P2))
		);
	}

}