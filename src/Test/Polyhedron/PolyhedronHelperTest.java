package Polyhedron;

import Geometry.Lines.Line3d;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PolyhedronHelperTest {

	private final Point3d P1 = new Point3d(11,12,13);
	private final Point3d P2 = new Point3d(21,22,23);
	private final Point3d P3 = new Point3d(31,32,33);
	private final Point3d P4 = new Point3d(41,42,43);
	private final Point3d Q1 = new Point3d(11+1e-6,12+1e-6,13+1e-6);


	@Test
	void getSharedEdge() {

		IPolyhedronHelper polyhedronHelper = new PolyhedronHelper();

		Point3d[] vertices = {P1,P2,P3,P4};
		int[][] faceIndices = {{1,2,0},{2,3,0},{3,1,0},{1,3,2},{2,3,1}};
		IPolyhedron polyhedron = new Polyhedron(vertices, null, faceIndices);

		Assertions.assertEquals(new Line3d(P3,P1), polyhedronHelper.getSharedEdge(polyhedron, 0,1));
		assertEquals(new Line3d(P4,P1), polyhedronHelper.getSharedEdge(polyhedron, 1,2));
		assertEquals(new Line3d(P4,P2), polyhedronHelper.getSharedEdge(polyhedron, 2,3));
		assertEquals(new Line3d(P3,P2), polyhedronHelper.getSharedEdge(polyhedron, 3,0));

		assertNull(polyhedronHelper.getSharedEdge(polyhedron, 2,4));
		assertNull(polyhedronHelper.getSharedEdge(polyhedron, 4,0));
	}

	@Test
	void removeVertexDuplicates() {

		IPolyhedronHelper polyhedronHelper = new PolyhedronHelper();

		Point3d[] vertices = {P1,P2,P3,Q1,P4};
		int[][] edgeIndices = {{1,0},{4,2},{2,3}};
		int[][] faceIndices = {{1,2,3},{2,4,0},{4,1,3},{1,4,2},{2,4,1}};
		IPolyhedron polyhedron = new Polyhedron(vertices, edgeIndices, faceIndices);

		polyhedronHelper.removeVertexDuplicates(polyhedron);

		assertArrayEquals(vertices, polyhedron.getVertices());
		assertArrayEquals(
			new int[][] {{1,0},{4,2},{2,0}},
			polyhedron.getEdgeIndices()
		);
		assertArrayEquals(
			new int[][] {{1,2,0},{2,4,0},{4,1,0},{1,4,2},{2,4,1}},
			polyhedron.getFaceIndices()
		);
	}

	@Test
	void generateEdgeIndicesFromFaceIndices() {

		IPolyhedronHelper polyhedronHelper = new PolyhedronHelper();

		Point3d[] vertices = {P1,P2,P3,P4};
		int[][] faceInds = {{1,2,0},{2,3,1}};
		IPolyhedron polyhedron = new Polyhedron(vertices, null, faceInds);

		polyhedronHelper.generateEdgeIndicesFromFaceIndices(polyhedron);

		int[][] expected = new int[][] {
			new int[] {1,2},
			new int[] {0,2},
			new int[] {0,1},
			new int[] {2,3},
			new int[] {1,3}
		};
		int[][] actual = polyhedron.getEdgeIndices();

		assertArrayEquals(expected, actual);
	}

	@Test
	void getEdgeAndFaceNormals() {

		IPolyhedronHelper polyhedronHelper = new PolyhedronHelper();

		Point3d leftTopFront = new Point3d(-1,1,1);
		Point3d rightTopFront = new Point3d(1,1,1);
		Point3d leftBottomFront = new Point3d(-1,-1,1);
		Point3d rightBottomFront = new Point3d(1,-1,1);
		Point3d leftTopBack = new Point3d(-1,1,-1);
		Point3d rightTopBack = new Point3d(1,1,-1);
		Point3d leftBottomBack = new Point3d(-1,-1,-1);
		Point3d rightBottomBack = new Point3d(1,-1,-1);

		Point3d[] vertices = {
			leftTopFront, // 0
			rightTopFront, // 1
			leftBottomFront, // 2
			rightBottomFront, // 3
			leftTopBack, // 4
			rightTopBack, // 5
			leftBottomBack, // 6
			rightBottomBack // 7
		};

		int[] leftFrontEdge = {0,2};
		int[] topEdge = {0,1};
		int[] isolatedEdge = {0,7};
		int[][] edgeIndices = {leftFrontEdge, topEdge, isolatedEdge};

		int[] leftFace = {2,0,4,6};
		int[] frontFace = {2,3,1,0};
		int[] degenerateFace = {2,3,2};
		int[][] faceIndices = {leftFace, frontFace, degenerateFace};

		IPolyhedron polyhedron = new Polyhedron(vertices, edgeIndices, faceIndices);

		Vector3d[] edgeNormals1 = new Vector3d[polyhedron.getNumEdges()];
		Vector3d[] edgeNormals2 = new Vector3d[polyhedron.getNumEdges()];
		Vector3d[] faceNormals = new Vector3d[polyhedron.getNumFaces()];
		polyhedronHelper.getEdgeAndFaceNormals(polyhedron, edgeNormals1, edgeNormals2, faceNormals);

		assertArrayEquals(
			new Vector3d[] {new Vector3d(-1,0,0), new Vector3d(0,0,1), null},
			faceNormals
		);

		assertEquals(edgeIndices.length, edgeNormals1.length);
		assertEquals(edgeIndices.length, edgeNormals2.length);

		assertArrayEquivalent(
			new Vector3d[] {edgeNormals1[0], edgeNormals2[0]},
			new Vector3d[] {new Vector3d(-1,0,0), new Vector3d(0,0,1)}
		);

		assertArrayEquals(
			new Vector3d[] {edgeNormals1[1], edgeNormals2[1]},
			new Vector3d[] {new Vector3d(0,0,1), null}
		);

		assertArrayEquals(
			new Vector3d[] {edgeNormals1[2], edgeNormals2[2]},
			new Vector3d[] {null, null}
		);
	}

	private void assertArrayEquivalent(Vector3d[] array1, Vector3d[] array2) {
		Set<Vector3d> set1 = new HashSet<>(Arrays.asList(array1));
		Set<Vector3d> set2 = new HashSet<>(Arrays.asList(array2));
		assertEquals(set1, set2);
	}
}