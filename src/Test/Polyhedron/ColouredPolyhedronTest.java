package Polyhedron;

import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class ColouredPolyhedronTest {

	private final Point3d P1 = new Point3d(11,12,13);
	private final Point3d P2 = new Point3d(21,22,23);
	private final Point3d P3 = new Point3d(31,32,33);
	private final Point3d P4 = new Point3d(41,42,43);


	@Test
	void defaultConstructor() {
		IColouredPolyhedron polyhedron = new ColouredPolyhedron();
		assertArrayEquals(new Point3d[0], polyhedron.getVertices());
		assertArrayEquals(new int[0][0], polyhedron.getEdgeIndices());
		assertArrayEquals(new int[0][0], polyhedron.getFaceIndices());
		assertArrayEquals(new Color[0], polyhedron.getFaceColours());
	}

	@Test
	void copyConstructor() {
		Point3d[] vertices = {P1,P2,P3};
		int[][] edgeIndices = {{0,2},{2,1}};
		int[][] faceIndices = {{0,1,2},{2,0,1},{1,2,0}};

		Color[] faceColours1 = {
			new Color(1,2,3),
			null,
			new Color(4,5,6,7)
		};
		IColouredPolyhedron polyhedron1 = new ColouredPolyhedron(vertices, edgeIndices, faceIndices, faceColours1);
		assertArrayEquals(faceColours1, polyhedron1.getFaceColours());

		Color[] faceColours2 = {
			new Color(1,2,3),
			new Color(4,5,6,7)
		};
		assertThrows(
			RuntimeException.class,
			() -> new ColouredPolyhedron(vertices, edgeIndices, faceIndices, faceColours2)
		);

		Color[] faceColours3 = null;
		IColouredPolyhedron polyhedron3 = new ColouredPolyhedron(vertices, edgeIndices, faceIndices, faceColours3);
		assertArrayEquals(new Color[3], polyhedron3.getFaceColours());
	}

	@Test
	void valueConstructor() {
		Point3d[] vertices = {P1,P2,P3};
		int[][] edgeIndices = {{0,2},{2,1}};
		int[][] faceIndices = {{0,1,2},{2,0,1},{1,2,0}};
		Color[] faceColours = {
			new Color(1,2,3),
			null,
			new Color(4,5,6,7)
		};
		IColouredPolyhedron polyhedron = new ColouredPolyhedron(vertices, edgeIndices, faceIndices, faceColours);

		IColouredPolyhedron copy = new ColouredPolyhedron(polyhedron);
		assertArrayEquals(vertices, copy.getVertices());
		assertArrayEquals(edgeIndices, copy.getEdgeIndices());
		assertArrayEquals(faceIndices, copy.getFaceIndices());
		assertArrayEquals(faceColours, copy.getFaceColours());
	}

	@Test
	void getFaceColours() {

		Point3d[] verts = new Point3d[8];
		for(int i=0; i<8; i++) verts[i] = new Point3d(0,0,0);

		int[][] faceInds1 = null;
		Color[] faceColours1 = null;

		int[][] faceInds2 = {};
		Color[] faceColours2 = null;

		int[][] faceInds3 = {{1,2,3},{4,5,6,7}};
		Color[] faceColours3 = null;

		int[][] faceInds4 = {{1,2,3},{4,5,6,7}};
		Color[] faceColours4 = {new Color(1,2,3), null};

		int[][] faceInds5 = {{1,2,3},{4,5,6,7}};
		Color[] faceColours5 = {new Color(1,2,3), new Color(4,5,6,7)};

		IColouredPolyhedron polyhedron1 = new ColouredPolyhedron(verts, null, faceInds1, faceColours1);
		assertArrayEquals(new Color[0], polyhedron1.getFaceColours());

		IColouredPolyhedron polyhedron2 = new ColouredPolyhedron(verts, null, faceInds2, faceColours2);
		assertArrayEquals(new Color[0], polyhedron2.getFaceColours());

		IColouredPolyhedron polyhedron3 = new ColouredPolyhedron(verts, null, faceInds3, faceColours3);
		assertArrayEquals(new Color[2], polyhedron3.getFaceColours());

		IColouredPolyhedron polyhedron4 = new ColouredPolyhedron(verts, null, faceInds4, faceColours4);
		assertArrayEquals(faceColours4, polyhedron4.getFaceColours());

		IColouredPolyhedron polyhedron5 = new ColouredPolyhedron(verts, null, faceInds5, faceColours5);
		assertArrayEquals(faceColours5, polyhedron5.getFaceColours());
	}

	@Test
	void getFaceColour() {

		Point3d[] verts = new Point3d[8];
		for(int i=0; i<8; i++) verts[i] = new Point3d(0,0,0);

		int[][] faceInds1 = null;
		Color[] faceColours1 = null;

		int[][] faceInds2 = {};
		Color[] faceColours2 = null;

		int[][] faceInds3 = {{1,2,3},{4,5,6,7}};
		Color[] faceColours3 = null;

		int[][] faceInds4 = {{1,2,3},{4,5,6,7}};
		Color[] faceColours4 = {new Color(1,2,3), null};

		int[][] faceInds5 = {{1,2,3},{4,5,6,7}};
		Color[] faceColours5 = {new Color(1,2,3), new Color(4,5,6,7)};

		IColouredPolyhedron polyhedron1 = new ColouredPolyhedron(verts, null, faceInds1, faceColours1);
		assertThrows(RuntimeException.class, () -> polyhedron1.getFaceColour(0));

		IColouredPolyhedron polyhedron2 = new ColouredPolyhedron(verts, null, faceInds2, faceColours2);
		assertThrows(RuntimeException.class, () -> polyhedron2.getFaceColour(0));

		IColouredPolyhedron polyhedron3 = new ColouredPolyhedron(verts, null, faceInds3, faceColours3);
		assertThrows(RuntimeException.class, () -> polyhedron3.getFaceColour(-1));
		assertNull(polyhedron3.getFaceColour(0));
		assertNull(polyhedron3.getFaceColour(1));
		assertThrows(RuntimeException.class, () -> polyhedron3.getFaceColour(2));

		IColouredPolyhedron polyhedron4 = new ColouredPolyhedron(verts, null, faceInds4, faceColours4);
		assertThrows(RuntimeException.class, () -> polyhedron4.getFaceColour(-1));
		assertEquals(faceColours4[0], polyhedron4.getFaceColour(0));
		assertNull(polyhedron4.getFaceColour(1));
		assertThrows(RuntimeException.class, () -> polyhedron4.getFaceColour(2));

		IColouredPolyhedron polyhedron5 = new ColouredPolyhedron(verts, null, faceInds5, faceColours5);
		assertThrows(RuntimeException.class, () -> polyhedron5.getFaceColour(-1));
		assertEquals(faceColours5[0], polyhedron5.getFaceColour(0));
		assertEquals(faceColours5[1], polyhedron5.getFaceColour(1));
		assertThrows(RuntimeException.class, () -> polyhedron5.getFaceColour(2));
	}

	@Test
	void equals() {

		Point3d[] vertices = {P1,P2,P3,P4};
		int[][] edgeInds = {{1,0},{3,2}};
		int[][] faceInds = {{1,2,0},{2,3,1}};

		Color[] faceColours1 = null;
		Color[] faceColours2 = {};
		Color[] faceColours3 = {Color.red};
		Color[] faceColours4 = {Color.red, Color.blue};
		Color[] faceColours5 = {Color.red, null};
		Color[] faceColours6 = {Color.red, Color.blue, Color.green};
		Color[][] faceColours = {faceColours1, faceColours2, faceColours3, faceColours4, faceColours5, faceColours6};

		IColouredPolyhedron polyhedron00 = new ColouredPolyhedron(vertices, edgeInds, faceInds, faceColours[3]);
		for(int i=0; i<faceColours.length; i++) {
			IColouredPolyhedron other;
			try {
				other = new ColouredPolyhedron(vertices, edgeInds, faceInds, faceColours[i]);
			}
			catch(Exception e) {
				assert(faceColours==null || faceColours.length!=faceInds.length);
				continue;
			}
			if (i == 3) assertEquals(polyhedron00, other);
			else assertNotEquals(polyhedron00, other);
		}
	}

	@Test
	void equals_compareWithNullFaceColour() {

		Point3d[] vertices = {P1,P2,P3,P4};
		int[][] edgeInds = {{1,0},{3,2}};
		int[][] faceInds = {{1,2,0},{2,3,1}};

		Color[] faceColours1 = null;
		Color[] faceColours2 = {};
		Color[] faceColours3 = {Color.red};
		Color[] faceColours4 = {Color.red, Color.blue};
		Color[] faceColours5 = {Color.red, null};
		Color[] faceColours6 = {Color.red, Color.blue, Color.green};
		Color[][] faceColours = {faceColours1, faceColours2, faceColours3, faceColours4, faceColours5, faceColours6};

		IColouredPolyhedron polyhedron00 = new ColouredPolyhedron(vertices, edgeInds, faceInds, faceColours[4]);
		for(int i=0; i<faceColours.length; i++) {
			IColouredPolyhedron other;
			try {
				other = new ColouredPolyhedron(vertices, edgeInds, faceInds, faceColours[i]);
			}
			catch(Exception e) {
				assert(faceColours==null || faceColours.length!=faceInds.length);
				continue;
			}
			if (i == 4) assertEquals(polyhedron00, other);
			else assertNotEquals(polyhedron00, other);
		}
	}
}