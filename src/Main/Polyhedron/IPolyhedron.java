package Polyhedron;

import Geometry.Lines.ILine3d;
import Geometry.Polygons.IPolygon3d;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

/**
 * A polyhedron with vertices and indices representing its edges and faces.
 */
public interface IPolyhedron {

	/**
	 * Gets the number of vertices.
	 * @return The number of vertices.
	 */
	int getNumVertices();

	/**
	 * Gets a copy of the vertices.
	 * @return A copy of the vertices.
	 */
	Point3d[] getVertices();

	/**
	 * Gets the specified vertex.
	 * @param index The vertex index.
	 * @return The specified vertex.
	 */
	Point3d getVertex(int index);

	/**
	 * Gets the number of edges.
	 * @return The number of edges.
	 */
	int getNumEdges();

	/**
	 * Gets a copy of the edge indices.
	 * @return A copy of the edge indices.
	 */
	int[][] getEdgeIndices();

	/**
	 * Gets a copy of the edges.
	 * @return A copy of the edges.
	 */
	ILine3d[] getEdges();

	/**
	 * Gets the specified edge.
	 * @param index The edge index.
	 * @return The specified edge.
	 */
	ILine3d getEdge(int index);

	/**
	 * Gets the number of faces.
	 * @return The number of faces.
	 */
	int getNumFaces();

	/**
	 * Gets a copy of the face indices.
	 * @return A copy of the face indices.
	 */
	int[][] getFaceIndices();

	/**
	 * Gets a copy of the faces.
	 * @return A copy of the faces.
	 */
	IPolygon3d[] getFaces();

	/**
	 * Gets the specified face.
	 * @param index The face index.
	 * @return The specified face.
	 */
	IPolygon3d getFace(int index);

	/**
	 * Applies a 4x4 affine transformation.
	 * @param matrix The 4x4 affine transformation matrix.
	 */
	void transform(Matrix4d matrix);

	@Override
	boolean equals(Object obj);
}
