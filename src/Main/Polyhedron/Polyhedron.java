package Polyhedron;

import Geometry.Lines.ILine3d;
import Geometry.Lines.Line3d;
import Geometry.Polygons.IPolygon3d;
import Geometry.Polygons.Polygon3d;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

/**
 * A polyhedron with vertices and indices representing its edges and faces.
 */
public class Polyhedron implements IPolyhedron {

	/**
	 * An array of 3D vertices.
	 */
	private Point3d[] m_vertices;

	/**
	 * An array of edge indices, each element being the two vertex indices of an edge.
	 */
	private int[][] m_edgeIndices;

	/**
	 * An array of face indices, each element being an array of integers corresponding to
	 * vertex indices going counterclockwise around a face, as seen from the outside of the polyhedron.
	 */
	private int[][] m_faceIndices;

	/**
	 * The default constructor.
	 */
	public Polyhedron() {
		set(new Point3d[0], new int[0][0], new int[0][0]);
	}

	/**
	 * Constructs a polyhedron from vertices, edges, and faces.
	 * @param vertices The polyhedron's vertices.
	 * @param edgeIndices An array of edge indices, each element being the two vertex indices of an edge.
	 * @param faceIndices An array of face indices, each element being an array of integers corresponding to
	 * vertex indices going counterclockwise around a face, as seen from the outside of the polyhedron.
	 */
	public Polyhedron(Point3d[] vertices, int[][] edgeIndices, int[][] faceIndices) {
		set(
			vertices==null ? new Point3d[0] : vertices,
			edgeIndices==null ? new int[0][0] : edgeIndices,
			faceIndices==null ? new int[0][0] : faceIndices
		);
	}

	/**
	 * The copy constructor.
	 * @param polyhedron The polyhedron to copy from.
	 */
	public Polyhedron(IPolyhedron polyhedron) {
		if(polyhedron==null) throw new RuntimeException();
		set(polyhedron.getVertices(), polyhedron.getEdgeIndices(), polyhedron.getFaceIndices());
	}

	/**
	 * Gets the number of vertices.
	 * @return The number of vertices.
	 */
	public int getNumVertices() {
		return m_vertices.length;
	}

	/**
	 * Gets a copy of the vertices.
	 * @return A copy of the vertices.
	 */
	public Point3d[] getVertices() {
		return deepCopy(m_vertices);
	}

	/**
	 * Gets the specified vertex.
	 * @param index The vertex index.
	 * @return The specified vertex.
	 */
	public Point3d getVertex(int index) {
		return new Point3d(m_vertices[index]);
	}

	/**
	 * Gets the number of edges.
	 * @return The number of edges.
	 */
	public int getNumEdges() {
		return m_edgeIndices.length;
	}

	/**
	 * Gets a copy of the edge indices.
	 * @return A copy of the edge indices.
	 */
	public int[][] getEdgeIndices() {
		return deepCopy(m_edgeIndices);
	}

	/**
	 * Gets a copy of the edges.
	 * @return A copy of the edges.
	 */
	public ILine3d[] getEdges() {
		int n = getNumEdges();
		ILine3d[] edges = new ILine3d[n];
		for(int i=0; i<n; i++) edges[i] = getEdge(i);
		return edges;
	}

	/**
	 * Gets the specified edge.
	 * @param index The edge index.
	 * @return The specified edge.
	 */
	public ILine3d getEdge(int index) {
		int[] indices = m_edgeIndices[index];
		if(indices.length!=2) throw new RuntimeException();
		ILine3d edge = new Line3d(m_vertices[indices[0]], m_vertices[indices[1]]);
		return edge;
	}

	/**
	 * Gets the number of faces.
	 * @return The number of faces.
	 */
	public int getNumFaces() {
		return m_faceIndices.length;
	}

	/**
	 * Gets a copy of the face indices.
	 * @return A copy of the face indices.
	 */
	public int[][] getFaceIndices() {
		return deepCopy(m_faceIndices);
	}

	/**
	 * Gets a copy of the faces.
	 * @return A copy of the faces.
	 */
	public IPolygon3d[] getFaces() {
		int n = getNumFaces();
		IPolygon3d[] faces = new IPolygon3d[n];
		for(int i=0; i<n; i++) {
			faces[i] = getFace(i);
		}
		return faces;
	}

	/**
	 * Gets the specified face.
	 * @param index The face index.
	 * @return The specified face.
	 */
	public IPolygon3d getFace(int index) {
		int[] indices = m_faceIndices[index];
		int n = indices.length;
		Point3d[] verts = new Point3d[n];
		for(int i=0; i<n; i++) {
			verts[i] = new Point3d(m_vertices[indices[i]]);
		}
		return new Polygon3d(verts);
	}

	/**
	 * Applies a 4x4 affine transformation.
	 * @param matrix The 4x4 affine transformation matrix.
	 */
	public void transform(Matrix4d matrix) {
		if(m_vertices==null) throw new RuntimeException();
		for(int i=0; i<m_vertices.length; i++) {
			Point3d p = m_vertices[i];
			matrix.transform(p);
			m_vertices[i] = p;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!IPolyhedron.class.isAssignableFrom(obj.getClass())) return false;
		return equals((IPolyhedron) obj);
	}

	/**
	 * Sets the vertices, edge indices, and face indices.
	 * @param vertices The vertices.
	 * @param edgeIndices The edge indices.
	 * @param faceIndices The face indices.
	 * @throws RuntimeException if edge indices are invalid.
	 */
	protected void set(Point3d[] vertices, int[][] edgeIndices, int[][] faceIndices) {
		if(vertices==null || edgeIndices==null || faceIndices==null) throw new RuntimeException();
		validateEdges(edgeIndices);
		m_vertices = deepCopy(vertices);
		m_edgeIndices = deepCopy(edgeIndices);
		m_faceIndices = deepCopy(faceIndices);
	}

	/**
	 * Compares two polyhedra.
	 * @param polyhedron The polyhedron to compare to.
	 * @return Whether they have the same vertices, edge indices, and face indices.
	 */
	private boolean equals(IPolyhedron polyhedron) {
		Point3d[] vertices = polyhedron.getVertices();
		int[][] edgeIndices = polyhedron.getEdgeIndices();
		int[][] faceIndices = polyhedron.getFaceIndices();

		// compare vertices
		if(m_vertices.length != vertices.length) return false;
		for(int i=0; i<m_vertices.length; i++) {
			if(m_vertices[i].distance(vertices[i]) > 1e-5) return false;
		}

		// compare edge indices
		if(m_edgeIndices.length != edgeIndices.length) return false;
		for(int i=0; i<m_edgeIndices.length; i++) {
			// don't need to compare m_edgeIndices[i].length and edgeIndices[i].length
			// since they must be 2
			for(int j=0; j<2; j++) {
				if(m_edgeIndices[i][j] != edgeIndices[i][j]) {
					return false;
				}
			}
		}

		// compare face indices
		if(m_faceIndices.length != faceIndices.length) return false;
		for(int i=0; i<m_faceIndices.length; i++) {
			if(m_faceIndices[i].length != faceIndices[i].length) return false;
			for(int j=0; j<m_faceIndices[i].length; j++) {
				if(m_faceIndices[i][j] != faceIndices[i][j]) return false;
			}
		}

		return true;
	}

	/**
	 * Ensures that each edge index contains exactly two vertex indices.
	 * @param edgeIndices The edge indices.
	 * @throws RuntimeException if edge indices are invalid.
	 */
	private void validateEdges(int[][] edgeIndices) {
		int n = edgeIndices.length;
		for(int i=0; i<n; i++) {
			if(edgeIndices[i]==null || edgeIndices[i].length != 2) throw new RuntimeException("invalid edge indices");
		}
	}

	/**
	 * Creates a deep copy of the points.
	 * @param points The points to copy.
	 * @return A copy of the points.
	 */
	private Point3d[] deepCopy(Point3d[] points) {
		int n = points.length;
		Point3d[] copy = new Point3d[n];
		for(int i=0; i<n; i++) {
			copy[i] = new Point3d(points[i]);
		}
		return copy;
	}

	/**
	 * Creates a deep copy of a 2D int array.
	 * @param intArrays The 2D int array to copy.
	 * @return A deep copy of the 2D int array.
	 */
	private int[][] deepCopy(int[][] intArrays) {
		int n = intArrays.length;
		int[][] copy = new int[n][];
		for(int i=0; i<n; i++) {
			copy[i] = deepCopy(intArrays[i]);
		}
		return copy;
	}

	/**
	 * Creates a deep copy of a 1D int array.
	 * @param ints The 1D int array to copy.
	 * @return A deep copy of the 1D int array.
	 */
	private int[] deepCopy(int[] ints) {
		int n = ints.length;
		int[] copy = new int[n];
		for(int i=0; i<n; i++) {
			copy[i] = ints[i];
		}
		return copy;
	}
}
