package Polyhedron;

import Geometry.Lines.ILine3d;

import javax.vecmath.Vector3d;

/**
 * A helper class for advanced manipulations of polyhedra.
 */
public interface IPolyhedronHelper {

	/**
	 * Gets the shared edge of two faces.
	 * @param polyhedron The polyhedron.
	 * @param faceInd1 The index of the first face.
	 * @param faceInd2 The indices of the second face.
	 * @return The shared edge as a line, or null if no edges are shared.
	 * <p>
	 * Note: There should only be one shared edges. The method handles this by returning the first shared
	 * edge found. The faces must be both outward facing or inward facing. For example, if face1 = {1,2,3} and
	 * face2 = {4,3,2} share the edge {2,3}, but face1 = {1,2,3} and face2 = {2,3,4} do not share edges.
	 * Also, this shared edge does not have to be one of the polyhedron's original edges.
	 * </p>
	 */	ILine3d getSharedEdge(IPolyhedron polyhedron, int faceInd1, int faceInd2);

	/**
	 * Removes vertex duplicates in a polyhedron by remapping edge and face indices. The actual duplicate
	 * vertices are not deleted.
	 * @param polyhedron The polyhedron.
	 */
	void removeVertexDuplicates(IPolyhedron polyhedron);

	/**
	 * Computes the edge indices associated with the face.
	 * @param polyhedron The polyhedron.
	 */
	void generateEdgeIndicesFromFaceIndices(IPolyhedron polyhedron);

	/**
	 * Gets edge and face outward normal vectors.
	 * @param polyhedron The polyhedron.
	 * @param edgeNormals1 The first normal vectors associated with the edges.
	 * @param edgeNormals2 The second normal vectors associated with the edges.
	 * @param faceNormals The normal vectors associated with the faces.
	 */
	void getEdgeAndFaceNormals(
		IPolyhedron polyhedron,
		Vector3d[] edgeNormals1,
		Vector3d[] edgeNormals2,
		Vector3d[] faceNormals
	);
}
