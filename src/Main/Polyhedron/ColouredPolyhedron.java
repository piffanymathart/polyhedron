package Polyhedron;

import javax.vecmath.Point3d;
import java.awt.*;

/**
 * A polyhedron with some coloured faces.
 */
public class ColouredPolyhedron extends Polyhedron implements IColouredPolyhedron {

	/**
	 * An array of face colours (elements may be null).
	 */
	private Color[] m_faceColours;

	/**
	 * A helper class for removing vertex duplicates.
	 */
	private IPolyhedronHelper m_polyhedronHelper = new PolyhedronHelper();

	/**
	 * The default constructor.
	 */
	public ColouredPolyhedron() {
		super();
		set(
			new Point3d[0],
			new int[0][0],
			new int[0][0],
			new Color[0]
		);
	}

	/**
	 * The copy constructor.
	 * @param polyhedron The polyhedron to copy from.
	 */
	public ColouredPolyhedron(IColouredPolyhedron polyhedron) {

		super();
		set(
			polyhedron.getVertices(),
			polyhedron.getEdgeIndices(),
			polyhedron.getFaceIndices(),
			polyhedron.getFaceColours()
		);
	}

	/**
	 * Constructs a polyhedron from vertices, edge indices, and face indices.
	 * @param vertices The polyhedron's vertices.
	 * @param edgeIndices An array of edge indices, each element being the two vertex indices of an edge.
	 * @param faceIndices An array of face indices, each element being an array of integers corresponding to
	 * vertex indices going counterclockwise around a face, as seen from the outside of the polyhedron.
	 * @param faceColours The face colours.
	 */
	public ColouredPolyhedron(Point3d[] vertices, int[][] edgeIndices, int[][] faceIndices, Color[] faceColours) {

		set(
			vertices==null ? new Point3d[0] : vertices,
			edgeIndices==null ? new int[0][0] : edgeIndices,
			faceIndices==null ? new int[0][0] : faceIndices,
			faceColours==null ? new Color[faceIndices==null ? 0 : faceIndices.length] : faceColours
		);
	}

	/**
	 * Gets the face colours.
	 * @return A copy of the face colours.
	 */
	public Color[] getFaceColours() { return deepCopy(m_faceColours); }

	/**
	 * Gets the colour of the specified face.
	 * @param index The face index.
	 * @return The colour of the specified face.
	 */
	public Color getFaceColour(int index) {
		Color c = m_faceColours[index];
		return c==null ? null : deepCopy(c);
	}

	/**
	 * Checks if the object is equal to the coloured polyhedron.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the coloured polyhedron.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!IColouredPolyhedron.class.isAssignableFrom(obj.getClass())) return false;
		return equals((IColouredPolyhedron) obj);
	}

	/**
	 * Sets the vertices, edge indices, face indices, and face colours.
	 * @param vertices The vertices.
	 * @param edgeIndices The edge indices.
	 * @param faceIndices The face indices.
	 * @param faceColours The face colours.
	 * @throws RuntimeException if edge indices are invalid.
	 */
	protected void set(Point3d[] vertices, int[][] edgeIndices, int[][] faceIndices, Color[] faceColours) {

		if(vertices==null || edgeIndices==null || faceIndices==null || faceColours==null) throw new RuntimeException();
		if(faceIndices.length!=faceColours.length) throw new RuntimeException();

		set(vertices, edgeIndices, faceIndices);
		m_polyhedronHelper.removeVertexDuplicates(this);
		m_faceColours = deepCopy(faceColours);
	}

	/**
	 * Compares two coloured polyhedra.
	 * @param polyhedron The coloured polyhedron to compare to.
	 * @return Whether they have the same vertices, edge indices, face indices, and face colours.
	 */
	private boolean equals(IColouredPolyhedron polyhedron) {

		if(!super.equals(polyhedron)) return false;

		Color[] faceColours = polyhedron.getFaceColours();

		// compare face colours
		if(m_faceColours.length != faceColours.length) return false;
		for (int i = 0; i < m_faceColours.length; i++) {
			if(m_faceColours[i] == null) {
				if(faceColours[i]!=null) return false;
			}
			else {
				if(faceColours[i]==null) return false;
				else if (!m_faceColours[i].equals(faceColours[i])) return false;
			}
		}

		return true;
	}

	/**
	 * Creates a deep copy of the colours.
	 * @param colours The colours to copy.
	 * @return A deep copy of the colours.
	 */
	private Color[] deepCopy(Color[] colours) {
		if(colours==null) throw new RuntimeException();
		int n = colours.length;
		Color[] copy = new Color[n];
		for(int i=0; i<n; i++) {
			copy[i] = deepCopy(colours[i]);
		}
		return copy;
	}

	/**
	 * Creates a deep copy of the colour.
	 * @param c The colour to copy.
	 * @return A deep copy of the colour.
	 */
	private Color deepCopy(Color c) {
		if(c == null) return null;
		return new Color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha());
	}
}
