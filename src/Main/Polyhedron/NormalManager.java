package Polyhedron;

import Geometry.Lines.ILine3d;
import Geometry.Lines.Line3d;
import Geometry.Polygons.IPolygon3d;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.List;

/**
 * A helper class to manage normal vector calculations.
 */
public class NormalManager implements INormalManager {

	/**
	 * Gets the normalized outward normal vector of a polygon, assuming its
	 * vertices are given counterclockwise when viewed from outside.
	 * @param polygon The polygon.
	 * @return The normalized outward normal vector.
	 */
	public Vector3d getNormal(IPolygon3d polygon) {
		int n = polygon.size();
		if(n<3) throw new RuntimeException();
		List<ILine3d> edges = new ArrayList<>();
		Point3d p1 = polygon.get(0);
		for(int i=1; i<n; i++) {
			Point3d p2 = polygon.get(i);
			if(p1.distance(p2) > 1e-5) edges.add(new Line3d(p1,p2));
		}
		if(edges.size() < 2) throw new RuntimeException();
		ILine3d e1 = edges.get(0);
		for(int i=1; i<edges.size(); i++) {
			ILine3d e2 = edges.get(i);
			if(areColinear(e1, e2)) continue;
			return getNormal(toVector(e1), toVector(e2));
		}
		throw new RuntimeException();
	}

	/**
	 * Gets the normalized cross product of two vectors.
	 * @param v1 The first vector.
	 * @param v2 The second vector.
	 * @return The normalized cross product.
	 */
	private Vector3d getNormal(Vector3d v1, Vector3d v2) {
		Vector3d normal = new Vector3d();
		normal.cross(v1, v2);
		if(normal.length()==0) throw new ArithmeticException("Normal vector has length zero.");
		normal.normalize();
		return normal;
	}

	/**
	 * Checks if two lines are colinear.
	 * @param line1 The first line.
	 * @param line2 The second line.
	 * @return Whether the lines are colinear.
	 */
	private static boolean areColinear(ILine3d line1, ILine3d line2) {
		Vector3d v1 = toVector(line1);
		v1.normalize();
		Vector3d v2 = toVector(line2);
		v2.normalize();
		return Math.abs(v1.dot(v2)) > 1-1e-5;
	}

	/**
	 * Converts a line to a vector.
	 * @param line The line.
	 * @return The corresponding vector.
	 */
	private static Vector3d toVector(ILine3d line) {
		Vector3d v = new Vector3d(line.getP2());
		v.sub(line.getP1());
		return v;
	}
}
