package Polyhedron;

import Geometry.Lines.ILine3d;
import Geometry.Lines.Line3d;
import Geometry.Polygons.IPolygon3d;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.*;

/**
 * A helper class for advanced manipulations of polyhedra.
 */
public class PolyhedronHelper implements IPolyhedronHelper {

	/**
	 * Manages normal vector calculations.
	 */
	private static INormalManager m_normalManager = new NormalManager();

	/**
	 * Gets the shared edge of two faces.
	 * @param polyhedron The polyhedron.
	 * @param faceInd1 The index of the first face.
	 * @param faceInd2 The indices of the second face.
	 * @return The shared edge as a line, or null if no edges are shared.
	 * <p>
	 * Note: There should only be one shared edges. The method handles this by returning the first shared
	 * edge found. The faces must be both outward facing or inward facing. For example, if face1 = {1,2,3} and
	 * face2 = {4,3,2} share the edge {2,3}, but face1 = {1,2,3} and face2 = {2,3,4} do not share edges.
	 * Also, this shared edge does not have to be one of the polyhedron's original edges.
	 * </p>
	 */	public ILine3d getSharedEdge(IPolyhedron polyhedron, int faceInd1, int faceInd2) {

		Point3d[] vertices = polyhedron.getVertices();
		int[][] faceIndices = polyhedron.getFaceIndices();

		Set<List<Integer>> edgeIndSet1 = getFaceEdgeIndices(faceIndices[faceInd1], true);
		Set<List<Integer>> edgeIndSet2 = getFaceEdgeIndices(faceIndices[faceInd2], false);

		for(List<Integer> edgeInd : edgeIndSet1) {
			if(edgeIndSet2.contains(edgeInd)) {
				Point3d p1 = vertices[edgeInd.get(0)];
				Point3d p2 = vertices[edgeInd.get(1)];
				return new Line3d(p1, p2);
			}
		}
		return null;
	}

	/**
	 * Removes vertex duplicates in a polyhedron by remapping edge and face indices. The actual duplicate
	 * vertices are not deleted.
	 * @param polyhedron The polyhedron.
	 */
	public void removeVertexDuplicates(IPolyhedron polyhedron) {

		Point3d[] vertices = polyhedron.getVertices();
		int[][] edgeIndices = polyhedron.getEdgeIndices();
		int[][] faceIndices = polyhedron.getFaceIndices();
		Map<Integer,Integer> noDupVertexMap = getNoDupVertexMap(vertices);
		((Polyhedron)polyhedron).set(
			vertices,
			getNoDupIndices(edgeIndices, noDupVertexMap),
			getNoDupIndices(faceIndices, noDupVertexMap)
		);
	}

	/**
	 * Gets a set of integer pairs, each representing an edge, from a list of vertex indices that defines a
	 * face. The result will either be in the same or opposite order as the input.
	 * @param faceIndices An array of face indices, each element being an array of integers corresponding to
	 * vertex indices going counterclockwise around a face, as seen from the outside of the polyhedron.
	 * @param sameOrder Whether the results should be in the same or opposite order as the input.
	 * @return A set of integer pairs, each representing an edge, from a list of vertex indices that defines a face.
	 */
	private static Set<List<Integer>> getFaceEdgeIndices(int[] faceIndices, boolean sameOrder) {
		int n = faceIndices.length;
		Set<List<Integer>> edgeIndices = new LinkedHashSet<>();
		for(int i=0; i<n; i++) {
			int j = (i == n-1) ? 0 : i+1;
			int faceInd1 = faceIndices[i];
			int faceInd2 = faceIndices[j];
			if(faceInd1==faceInd2) continue;
			List<Integer> edgeInds = new ArrayList<>();
			edgeInds.add(sameOrder ? faceInd1 : faceInd2);
			edgeInds.add(sameOrder ? faceInd2 : faceInd1);
			if(!edgeIndices.contains(edgeInds)) edgeIndices.add(edgeInds);
		}
		return edgeIndices;
	}

	/**
	 * Gets the mapping between each vertex and its first occurrence in the vertex list.
	 * @param vertices The vertices.
	 * @return The vertex-to-first-occurrence mapping.
	 */
	private Map<Integer,Integer> getNoDupVertexMap(Point3d[] vertices) {

		double tol = 1e-5;
		Point3d[] roundVerts = roundValues(vertices, tol);

		Map<Point3d,Integer> posToIndMap = new HashMap<>();
		Map<Integer,Integer> noDupVertexMap = new HashMap<>();
		for(int i=0; i<roundVerts.length; i++) {
			Point3d p = roundVerts[i];
			if(posToIndMap.containsKey(p)) {
				int canonicalIndex = posToIndMap.get(p);
				noDupVertexMap.put(i, canonicalIndex);
			}
			else {
				posToIndMap.put(p, i);
				noDupVertexMap.put(i, i);
			}
		}

		return noDupVertexMap;
	}

	/**
	 * Gets edge/face indices without vertex duplicates given the no duplicate mapping.
	 * @param inds The indices from which to remove vertex duplicates.
	 * @param noDupVertexMap The vertex-to-first-occurrence mapping.
	 * @return The indices without vertex duplicates.
	 */
	private int[][] getNoDupIndices(int[][] inds, Map<Integer,Integer> noDupVertexMap) {
		int[][] newInds = new int[inds.length][];
		int n = inds.length;
		for(int i=0; i<n; i++) {
			newInds[i] = getNoDupIndex(inds[i], noDupVertexMap);
		}
		return newInds;
	}

	/**
	 * Gets edge/face index without vertex duplicates given the no duplicate mapping.
	 * @param ind The index from which to remove vertex duplicates.
	 * @param noDupVertexMap The vertex-to-first-occurrence mapping.
	 * @return The index without vertex duplicates.
	 */
	private static int[] getNoDupIndex(int[] ind, Map<Integer,Integer> noDupVertexMap) {
		int n = ind.length;
		int[] newInd = new int[n];
		for(int i=0; i<n; i++) {
			newInd[i] = noDupVertexMap.get(ind[i]);
		}
		return newInd;
	}

	/**
	 * Rounds a set of 3D coordinates to the specified tolerance.
	 * @param points The points.
	 * @param tol The tolerance.
	 * @return The rounded 3D coordinates.
	 */
	private static Point3d[] roundValues(Point3d[] points, double tol) {

		int n = points.length;
		double factor = Math.round(1.0/tol);
		Point3d[] newPts = new Point3d[n];
		for(int i=0; i<n; i++) {
			double x = points[i].x;
			double y = points[i].y;
			double z = points[i].z;
			double newX = Math.round(x*factor)/factor;
			double newY = Math.round(y*factor)/factor;
			double newZ = Math.round(z*factor)/factor;
			newPts[i] = new Point3d(newX, newY, newZ);
		}
		return newPts;
	}

	/**
	 * Computes the edge indices associated with the face.
	 * @param polyhedron The polyhedron.
	 */
	public void generateEdgeIndicesFromFaceIndices(IPolyhedron polyhedron) {
		int[][] faceIndices = polyhedron.getFaceIndices();
		Set<List<Integer>> edgeIndicesSet = new LinkedHashSet<>();
		for(int faceIndex=0; faceIndex<faceIndices.length; faceIndex++) {
			int n = faceIndices[faceIndex].length;
			for (int i = 0; i < n; i++) {
				int j = (i == n - 1) ? 0 : i + 1;
				int faceInd1 = faceIndices[faceIndex][i];
				int faceInd2 = faceIndices[faceIndex][j];
				if (faceInd1 == faceInd2) continue;
				List<Integer> edgeInds = new ArrayList<>();
				if (faceInd1 < faceInd2) {
					edgeInds.add(faceInd1);
					edgeInds.add(faceInd2);
				} else {
					edgeInds.add(faceInd2);
					edgeInds.add(faceInd1);
				}
				if (!edgeIndicesSet.contains(edgeInds)) edgeIndicesSet.add(edgeInds);
			}
		}

		int[][] edgeIndices = new int[edgeIndicesSet.size()][];
		int i=0;
		for(List<Integer> edgeInd : edgeIndicesSet) {
			edgeIndices[i] = new int[] {edgeInd.get(0), edgeInd.get(1)};
			i++;
		}

		((Polyhedron)polyhedron).set(
			polyhedron.getVertices(),
			edgeIndices,
			faceIndices
		);
	}

	/**
	 * Gets edge and face outward normal vectors.
 	 * @param polyhedron The polyhedron.
	 * @param edgeNormals1 The first normal vectors associated with the edges.
	 * @param edgeNormals2 The second normal vectors associated with the edges.
	 * @param faceNormals The normal vectors associated with the faces.
	 */
	public void getEdgeAndFaceNormals(IPolyhedron polyhedron, Vector3d[] edgeNormals1, Vector3d[] edgeNormals2, Vector3d[] faceNormals) {

		Map<List<Integer>,List<Vector3d>> faceEdgeFaceNormalMap = new HashMap<>();
		getFaceNormals(polyhedron, faceNormals, faceEdgeFaceNormalMap);
		getEdgeNormals(polyhedron, faceEdgeFaceNormalMap, edgeNormals1, edgeNormals2);
	}

	/**
	 * Gets the face outward normal vectors.
	 * @param polyhedron The polyhedron.
	 * @param faceNormals The outward face normal vectors.
	 * @param faceEdgeFaceNormalMap A face-normal-to-edge mapping.
	 */
	private static void getFaceNormals(
		IPolyhedron polyhedron,
		Vector3d[] faceNormals,
		Map<List<Integer>,List<Vector3d>> faceEdgeFaceNormalMap
	) {
		IPolygon3d[] faces = polyhedron.getFaces();
		int[][] faceIndices = polyhedron.getFaceIndices();
		faceEdgeFaceNormalMap.clear();

		for(int i=0; i<faces.length; i++) {
			try {
				faceNormals[i] = m_normalManager.getNormal(faces[i]);
			}
			catch(Exception e) {
				faceNormals[i] = null;
				continue;
			}
			addFaceNormalToEdgeMap(faceIndices[i], faceNormals[i], faceEdgeFaceNormalMap);
		}
	}

	/**
	 * Adds to the face-normal-to-edge mapping.
	 * @param faceIndex The face index.
	 * @param faceNormal The face normal.
	 * @param faceEdgeFaceNormalMap The face-normal-to-edge mapping to add to.
	 */
	private static void addFaceNormalToEdgeMap(
		int[] faceIndex,
		Vector3d faceNormal,
		Map<List<Integer>,List<Vector3d>> faceEdgeFaceNormalMap
	) {
		int numVerts = faceIndex.length;
		for(int j=0; j<numVerts; j++) {
			int k = (j==numVerts-1) ? 0 : j+1;
			int ind1 = faceIndex[j];
			int ind2 = faceIndex[k];
			List<Integer> edgeInds = getEdgeListIndices(ind1, ind2);
			if(faceEdgeFaceNormalMap.containsKey(edgeInds)) {
				List<Vector3d> normals = faceEdgeFaceNormalMap.get(edgeInds);
				normals.add(faceNormal);
				faceEdgeFaceNormalMap.put(edgeInds, normals);
			}
			else {
				List<Vector3d> normals = new ArrayList<>();
				normals.add(faceNormal);
				faceEdgeFaceNormalMap.put(edgeInds, normals);
			}
		}
	}

	/**
	 * Gets the edge outward normal vectors.
	 * @param polyhedron The polyhedron.
	 * @param faceEdgeFaceNormalMap A face-normal-to-edge mapping.
	 * @param edgeNormals1 The first outward face normal vectors.
	 * @param edgeNormals2 The second outward face normal vectors.
	 */
	private static void getEdgeNormals(
		IPolyhedron polyhedron,
		Map<List<Integer>,List<Vector3d>> faceEdgeFaceNormalMap,
		Vector3d[] edgeNormals1,
		Vector3d[] edgeNormals2
	) {
		int[][] edgeIndices = polyhedron.getEdgeIndices();
		for(int i=0; i<edgeIndices.length; i++) {
			int ind1 = edgeIndices[i][0];
			int ind2 = edgeIndices[i][1];
			List<Integer> edgeInds = getEdgeListIndices(ind1, ind2);
			if(faceEdgeFaceNormalMap.containsKey(edgeInds)) {
				List<Vector3d> normals = faceEdgeFaceNormalMap.get(edgeInds);
				Vector3d[] normalsArray = normals.toArray(new Vector3d[normals.size()]);
				edgeNormals1[i] = normalsArray[0];
				if(normalsArray.length > 1)	edgeNormals2[i] = normalsArray[1];
				else edgeNormals2[i] = null;
			}
			else {
				edgeNormals1[i] = null;
				edgeNormals2[i] = null;
			}
		}
	}

	/**
	 * Gets the edge index as an integer list.
	 * @param ind1 The first vertex index.
	 * @param ind2 The second vertex index.
	 * @return The edge index as an integer list.
	 */
	private static List<Integer> getEdgeListIndices(int ind1, int ind2) {
		int minInd = Math.min(ind1, ind2);
		int maxInd = Math.max(ind1, ind2);
		List<Integer> edgeInds = Arrays.asList(new Integer[] {minInd, maxInd});
		return edgeInds;
	}
}
