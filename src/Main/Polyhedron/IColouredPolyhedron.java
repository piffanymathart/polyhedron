package Polyhedron;

import java.awt.*;

/**
 * A polyhedron with some coloured faces.
 */
public interface IColouredPolyhedron extends IPolyhedron {

	/**
	 * Gets the face colours.
	 * @return A copy of the face colours.
	 */
	Color[] getFaceColours();

	/**
	 * Gets the colour of the specified face.
	 * @param index The face index.
	 * @return The colour of the specified face.
	 */
	Color getFaceColour(int index);

	/**
	 * Checks if the object is equal to the coloured polyhedron.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the coloured polyhedron.
	 */
	@Override
	boolean equals(Object obj);
}
