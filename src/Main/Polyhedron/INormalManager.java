package Polyhedron;

import Geometry.Polygons.IPolygon3d;

import javax.vecmath.Vector3d;

public interface INormalManager {

	Vector3d getNormal(IPolygon3d polygon);
}
